<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Medidas extends Model
{
    use HasFactory;

    protected $connection = 'medidas';
    protected $table = 'VMedidasProtec911';
    protected $primaryKey = 'idmedidas';
    public $incrementing = false;
    protected $keyType = 'string';
    public $timestamps = false;

    protected $fillable = ['idmedidas','idinvbenef','DescBeneficiario','CveColMun',' VMP911Colonia',
    'VMP911Calle','VMP911Numero','VMP911ObserAgregor','VMP911RedApoyo',
    'VMP911Agresores','VMP911Telefono'
    ];
}
