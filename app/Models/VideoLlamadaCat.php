<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class VideoLlamadaCat extends Model
{
    use HasFactory;
    protected $connection = 'videollamadas';
    protected $table = 'VideoLlamadasCategoria';
    public $timestamps = false;
    protected $fillable = [
        'categoria',
        'des_categoria',
        'color_categoria',
    ];
}
