<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $table = 'USUARIOS';
    protected $primaryKey = 'CveUsuario';
    public $incrementing = false;
    protected $keyType = 'string';
    public $timestamps = false;
    protected $fillable = [
        'CveUsuario' ,'NombreUsr' ,'PasswdUsr' ,'AcumUsr' ,'NumAcce' ,'TiemBloque' ,'UsrBloquea' ,'CveDirOrga' ,'CveNumEmp' ,'CveDeptoOr' ,'InicialesUsr' ,'correo'
    ];


    public function modulos()
    {
        return $this->belongsToMany(Modulo::class,'ACCESOSU','CveUsuario','CveModulo')->withPivot('Acceso');
    }

    public function moduloVllam()
    {
        return $this->belongsToMany(Modulo::class,'ACCESOSU','CveUsuario','CveModulo')->withPivot('Acceso','CveSubMod')->wherePivot('CveModulo','VLLAM');
    }
}
