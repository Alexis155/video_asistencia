<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SubModul extends Model
{
    use HasFactory;

    protected $table = 'SUBMODUL';
    protected $primaryKey = 'CveSubMod';
    public $incrementing = false;
    protected $keyType = 'string';
    public $timestamps = false;
    protected $fillable = [
        'CveModulo' ,'CveSubMod' ,'DesSubMod' ,'LinkSubMod'
    ];

    public function modelo()
    {
        return $this->belongsTo(Modulo::class,'CveModulo');
    }
}
