<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Despachador extends Model
{
    use HasFactory;

    protected $connection = 'videollamadas';
    protected $table = 'Despachador';
    protected $primaryKey = 'DesDespacha';
    public $incrementing = false;
    protected $keyType = 'string';
    public $timestamps = false;

    protected $fillable = ['DesDespacha','EstacionTraba','GeneraRep','Conocimiento',
    'Vista','DespachoActualiza','StatusDespachador','ClienteExterno',
    'SSPM','DespachadorAlias','IdDespachador','VideoLlamada'
    ];
}
