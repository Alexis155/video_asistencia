<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class VideoLlamada extends Model
{
    use HasFactory;
    protected $connection = 'videollamadas';
    protected $table = 'VideoLlamadas';
    public $timestamps = false;
    protected $fillable = [
        'room_name',
        'status',
        'usuario',
        'lat',
        'long',
        'DesDespacha',
        'video',
        'origen'
    ];
}
