<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Modulo extends Model
{
    use HasFactory;

    protected $primaryKey = 'CveModulo';
    public $incrementing = false;
    protected $keyType = 'string';
    public $timestamps = false;

    protected $fillable = [
        'CveModulo','DesModulo','PathModulo','BDModulo','BCModulo','Descripcio',
        'Habilitado','link','Nuevaventana','DetallesUso','CveKB','ModuloVersion','ModulosInfoTecnica'
    ];

    public function submodulos()
    {
        return $this->hasMany(SubModul::class,'CveModulo', 'CveModulo');
    }
    public function usuarios()
    {
        return $this->belongsToMany(User::class,'ACCESOSU','CveModulo','CveUsuario');
    }
}
