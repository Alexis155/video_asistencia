<?php

namespace App\Http\Controllers;

use App\Models\Call;
use App\Models\Despachador;
use App\Events\Call as EventsCall;
use App\Events\ContestarLLamada;
use App\Events\Transferir;
use App\Models\VideoLlamada;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Events\Callcrum as EventsCallCrum;
use App\Events\Calloperador as EventsCallOperador;
use App\Events\Callmedic as EventsCallMedic;
use App\Events\ContestarLLamadacrum;
use App\Events\ContestarLLamadaoperador;
use App\Events\ContestarLLamadamedic;

class CallController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('CreateCall');
    }

    public function main()
    {
        return view('main');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $name = $request->name;
        $videollamada = VideoLlamada::create([
            'room_name' => $name,
            'usuario' => $request->user,
            'status' => 0,
            'lat' => $request->lat,
            'long' => $request->long,
            'origen' => 1,
        ]);
        event(new EventsCall($videollamada));
        return ['status' => $videollamada];
    }

    public function storee(Request $request)
    {
        if($request->has('name')){
            $videollamadas = null;
            $videollamadas = VideoLlamada::where('status', 0)->where('room_name', $request->name)->orderBy('id')->get();
            $resultado['data'] = $videollamadas;
            if (count($videollamadas) > 0) {
                $resultado['resultado'] = 'false';
            }else if ((count($videollamadas) === 0)){
                $resultado['resultado'] = 'true';
                $name = $request->name;
                $videollamada = VideoLlamada::create([
                    'room_name' => $name,
                    'usuario' => $request->user,
                    'status' => 0,
                    'lat' => $request->lat,
                    'long' => $request->long,
                    'origen' => 2,
                ]);
                event(new EventsCall($videollamada));
            }
            return $resultado;
        }else {
            return $resultado['error'] = 'No contiene parametros';
        }
    }

    public function storeCrum(Request $request, $name, $user)
    {
        if($name != null){
            $videollamadas = null;
            $videollamadas = VideoLlamada::where('status', '!=', 2)->where('room_name', $name)->where('DesDespacha', 'CRUM')->orderBy('id')->get();
            //$resultado['data'] = $videollamadas;
             if (count($videollamadas) > 0) {
                $resultado['resultado'] = 'false';
                $resultado['error'] = 'Ya existe una videollamada con el nombre de la sala';
            }else if ((count($videollamadas) === 0)){
                $resultado['resultado'] = 'true';
                $videollamada = VideoLlamada::create([
                    'room_name' => $name,
                    'usuario' => $user,
                    'status' => 0,
                    'lat' => NULL,
                    'long' => NULL,
                    'DesDespacha' => 'CRUM',
                    'origen' => 1,
                ]);
                broadcast(new Transferir($videollamada));
                event(new EventsCall($videollamada));

            }
            return $resultado;
        }else {
            return $resultado['error'] = 'No contiene parametros';
        }
    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $call = VideoLlamada::find($id);
        $call->update([
            'status' => 1,
        ]);
        broadcast(new ContestarLLamada($call));
        return ['status' => 'success', 'llamada' => $call];
    }

    public function updateCrum(Request $request, $id)
    {
        $call = VideoLlamada::find($id);
        $call->update([
            'status' => 0,
        ]);
        broadcast(new ContestarLLamadacrum($call));
        return ['status' => 'success', 'llamada' => $call];
    }

    public function updateOperador(Request $request, $id)
    {
        $call = VideoLlamada::find($id);
        $call->update([
            'status' => 0,
        ]);
        broadcast(new ContestarLLamadaoperador($call));
        return ['status' => 'success', 'llamada' => $call];
    }

    public function updateMedic(Request $request, $id)
    {
        $call = VideoLlamada::find($id);
        $call->update([
            'status' => 0,
        ]);
        broadcast(new ContestarLLamadamedic($call));
        return ['status' => 'success', 'llamada' => $call];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function transferir(Request $request, $id)
    {
        $call = VideoLlamada::find($id);
        $call->update([
            'DesDespacha' => $request->DesDespacha
        ]);
        broadcast(new Transferir($call));
        return ['status' => 'success', 'llamada' => $call];
    }

    public function video(Request $request)
    {

        if($request->hasFile('video')){
            set_time_limit(0);
            ini_set('memory_limit', '9G');
            try{
                $video = $request->file('video')->storeAs('public/videos','llamada-'.$request->nameVideo.'.mp4');
                $videollamada = VideoLlamada::find($request->id);
                $videollamada->video = 'llamada-'.$request->nameVideo.'.mp4';
                $videollamada->save();
                return ['status' => 'success'];
            }catch(\Exception $e){
                return response()->json(['error' => $$e->getMessage()]);
            }

        }
    }
    public function historial(){
        $data  = [
            'vllamadas' => VideoLlamada::where('status',2)->get(),
        ];
        return view('historial')->with($data);
    }
    public function terminar($id){
        $vl = VideoLlamada::find($id);
        $vl->status = 2;
        $vl->save();
        return ['status' => 'success', 'llamada' => $vl];
    }

    public function uploadVideo(Request $request){
        set_time_limit(0);
        ini_set('memory_limit', '9G');
        if($request->hasFile('video')){
            try{
                $video = $request->file('video')->storeAs('public/videos','llamda-'.$request->title.'.mp4');
                return ['status' => 'success'];
            }catch(\Exception $e){
                return ['status' => 'error', 'mesaje' => $e->getMessage()];
            }

        }
    }
    
    public function getCalls(Request $request, $roomname)
    {
        $videollamadas = null;
        $videollamadas = VideoLlamada::where('status', 0)->where('room_name', $roomname)->where('origen', 2)->orderBy('id')->get();
        return $videollamadas;
    }
}
