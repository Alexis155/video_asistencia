<?php

namespace App\Http\Controllers\Login;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function index()
    {
        return view('auth.login');
    }
    public function login(Request $request)
    {
        $user = User::with('moduloVllam')->where('CveUsuario', $request->CveUsuario)->first();
        if($user){
            if($request->CveUsuario == rtrim($user->CveUsuario) && $request->PasswdUsr == rtrim($user->PasswdUsr)){
                Auth::login($user);
                return redirect('/home');
            }else{
                return redirect()->back()->withErrors(['usuario' => 'Usuario y/o Contraseña incorrectos']);
            }
        }else{
            return redirect()->back()->withErrors(['usuario' => 'Usuario y/o Contraseña incorrectos']);
        }
    }
    public function autologin(Request $request)
    {
        $user = User::with('moduloVllam')->where('CveUsuario', $request->CveUsuario)->first();
        if($user){
            if($request->CveUsuario == rtrim($user->CveUsuario) && $request->PasswdUsr == rtrim($user->PasswdUsr)){
                Auth::login($user);
                return true;
            }else{
                return 0;
            }
        }else{
            return 0;
        }
    }
    public function logout()
    {
        Auth::logout();
        return redirect('/');
    }
}
