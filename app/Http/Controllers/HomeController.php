<?php

namespace App\Http\Controllers;

use App\Models\Call;
use App\Models\Despachador;
use App\Models\User;
use App\Models\VideoLlamada;
use App\Models\VideoLlamadaCat;
use App\Models\Medidas;
use App\Models\Link;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $user = User::with('moduloVllam')->where('CveUsuario',auth()->user()->CveUsuario)->first();
        return view('home')->with('user', $user);
    }

    public function getCalls()
    {
        $videollamadas = null;
        foreach(auth()->user()->moduloVllam as $modulo){
            if($modulo->pivot->CveSubMod === 'RECEP'){
                $videollamadas = VideoLlamada::where('status', 0)->where('origen',1)->orderBy('id')->get();
            }
            if($modulo->pivot->CveSubMod == 'DG621'){
                 $videollamadas = VideoLlamada::where('DesDespacha','DGUAR621')->where('status',1)->where('origen',1)->orderBy('id')->get();
            }
            if($modulo->pivot->CveSubMod == 'DG622'){
                 $videollamadas = VideoLlamada::where('DesDespacha','DGUAR622')->where('status',1)->where('origen',1)->orderBy('id')->get();
            }
            if($modulo->pivot->CveSubMod == 'DG623'){
                $videollamadas = VideoLlamada::where('DesDespacha','DGUAR625')->where('status',1)->where('origen',1)->orderBy('id')->get();
            }
            if($modulo->pivot->CveSubMod == 'DG624'){
                $videollamadas = VideoLlamada::where('DesDespacha','DGUAR624')->where('status',1)->where('origen',1)->orderBy('id')->get();
            }
            if( trim($modulo->pivot->CveSubMod) == 'CRUM'){
                $videollamadas = VideoLlamada::where('DesDespacha','CRUM')->where('status', '!=', 2)->where('origen',1)->orderBy('id')->get();
            }
            if( trim($modulo->pivot->CveSubMod) == 'MEDIC'){
                $videollamadas = VideoLlamada::where('DesDespacha','CRUM')->where('status', '!=', 2)->where('origen',1)->orderBy('id')->get();
            }
        }
        return $videollamadas;
    }
    public function getColors()
    {
        $colors = null;
        $colors = VideoLlamadaCat::get();

        return $colors;
    }
    public function despachadores()
    {
        $despachadores = Despachador::where('VideoLlamada',1)->get();
        return $despachadores;
    }
    public function getMedidas() 
    {
        $medidas = Medidas::where('idmedidas', '>', 1)->get(); 
        return $medidas;
    }
    public function getLink() 
    {
        $link = Link::orderBy('id_link911', 'DESC')->first();
        return $link['link911'];
    }

    public function findMedida(Request $request, $mp)
    {
        $resultado = [];
        $medida = Medidas::where('idmedidas', $mp)->get();
        if (count($medida) === 0) {
            $resultado['resultado'] = 'false';
        }else if ((count($medida) > 0)){
            $resultado['resultado'] = 'true';
            $resultado['beneficiario'] = $medida[0]['DescBeneficiario'];
            $resultado['telefono'] = $medida[0]['VMP911Telefono'];
            $resultado['colonia'] = $medida[0]['CveColMun'];
            $resultado['VMP911Colonia'] = $medida[0]['VMP911Colonia'];
            $resultado['calle'] = $medida[0]['VMP911Calle'];
            $resultado['número'] = $medida[0]['VMP911Numero'];
            $resultado['redApoyo'] = $medida[0]['VMP911RedApoyo'];
            $resultado['agresores'] = $medida[0]['VMP911Agresores'];
            $resultado['descripción'] = $medida[0]['VMP911ObserAgresor'];
        }
        return $resultado;
    }
    public function findMedidaBenef(Request $request, $mp, $benef)
    {
        $resultado = [];
        $medida = Medidas::where('idmedidas', $mp)->where('idinvbenef', $benef)->get();
        if (count($medida) === 0) {
            $resultado['resultado'] = 'false';
        }else if ((count($medida) > 0)){
            $resultado['resultado'] = 'true';
            $resultado['beneficiario'] = $medida[0]['DescBeneficiario'];
            $resultado['telefono'] = $medida[0]['VMP911Telefono'];
            $resultado['colonia'] = $medida[0]['CveColMun'];
            $resultado['VMP911Colonia'] = $medida[0]['VMP911Colonia'];
            $resultado['calle'] = $medida[0]['VMP911Calle'];
            $resultado['número'] = $medida[0]['VMP911Numero'];
            $resultado['redApoyo'] = $medida[0]['VMP911RedApoyo'];
            $resultado['agresores'] = $medida[0]['VMP911Agresores'];
            $resultado['descripción'] = $medida[0]['VMP911ObserAgresor'];
        }
        return $resultado;
    }
}
