
<!DOCTYPE html>
<html>

<head>
  <meta charset="UTF-8">
  <title>VideoChat</title>
  <link rel="stylesheet" href="styles.css">
  <script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/js/all.min.js"></script>
  <style>
      #video {
        border: 1px solid #999;
        width: 98%;
        max-width: 860px;
    }

    .error {
    color: red;
    }

    .warn {
    color: orange;
    }

    .info {
    color: darkgreen;
    }
  </style>
</head>

<body>
    {{-- <video autoplay id="video" ></video>
    <button type="button" id="boton">Grabar</button> --}}
    <p>This example shows you the contents of the selected part of your display.
        Click the Start Capture button to begin.</p>

        <p><button id="start">Start Capture</button>&nbsp;<button id="stop">Stop Capture</button></p>

        <video id="video" autoplay></video>
        <br>

        <strong>Log:</strong>
        <br>
        <pre id="log"></pre>
</body>
    <script>
        const videoElem = document.getElementById("video");
        const logElem = document.getElementById("log");
        const startElem = document.getElementById("start");
        const stopElem = document.getElementById("stop");

        var displayMediaOptions = {
        video: {
            cursor: "always"
        },
        audio: true
        };

        startElem.addEventListener("click", function(evt) {
            startCapture();
        }, false);

        stopElem.addEventListener("click", function(evt) {
            stopCapture();
        }, false);

        console.log = msg => logElem.innerHTML += `${msg}<br>`;
        console.error = msg => logElem.innerHTML += `<span class="error">${msg}</span><br>`;
        console.warn = msg => logElem.innerHTML += `<span class="warn">${msg}<span><br>`;
        console.info = msg => logElem.innerHTML += `<span class="info">${msg}</span><br>`;
        async function startCapture() {
            logElem.innerHTML = "";
            try {
                videoElem.srcObject = await navigator.mediaDevices.getDisplayMedia(displayMediaOptions);
                dumpOptionsInfo();
            } catch(err) {
                console.error("Error: " + err);
            }
        }

        function stopCapture(evt) {
            let tracks = videoElem.srcObject.getTracks();

            tracks.forEach(track => track.stop());
            videoElem.srcObject = null;
        }

        function dumpOptionsInfo() {
            const videoTrack = videoElem.srcObject.getVideoTracks()[0];

            console.info("Track settings:");
            console.info(JSON.stringify(videoTrack.getSettings(), null, 2));
            console.info("Track constraints:");
            console.info(JSON.stringify(videoTrack.getConstraints(), null, 2));
        }
    </script>
{{-- <script>
    let video = document.querySelector("#video");
    var displayMediaOptions = {
        video:true,
        audio: true
    };
    document.querySelector("#boton").addEventListener('click', function(ev){
        navigator.mediaDevices.getDisplayMedia(displayMediaOptions).then(record).catch(err => console.log(err));

    });
    let chunks = [];
    function record(stream){
        console.log(stream);
        video.srcObject = stream;

        let mediaRecorder = new MediaRecorder(stream,{
            mimeType: 'video/webm;codecs=h264'
        });

        mediaRecorder.start();
        mediaRecorder.ondataavailable = function(e){
            console.log(e.data);
            chunks.push(e.data);
        }
        mediaRecorder.onstop = function(){
        alert('finalizo la grabacion');
        let blob = new Blob(chunks, {type:"video/webm"});
        chunks = [];
        download(blob);
        }

        setTimeout(()=>mediaRecorder.stop(), 10000)
    }
    function download(blob){
        var link = document.createElement('a');
        link.href = window.URL.createObjectURL(blob);
        link.setAttribute("download", "video_recorded.webm");
        link.style.display = 'none';
        document.body.appendChild(link);
        link.click();
        link.remove();
    }
</script> --}}
</html>
