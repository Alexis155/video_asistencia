@extends('layouts.app')
@section('content')
<div class="container-fluid">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h2>Historial de llamadas</h2>
                </div>
                <div class="card-body">
                    <div class="table-responsive mt-4">
                        <table class="table text-center">
                        <thead>
                            <tr>
                                <th scope="col">#</th>
                                <th scope="col">Llamada</th>
                                <th scope="col">Usuario</th>
                                <th scope="col-5">Evidencia</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ( $vllamadas as $vl)
                                <tr>
                                    <th scope="row">cidudadano {{ $vl->usuario }} {{ $vl->id }}</th>
                                    <td>{{ $vl->room_name }}</td>
                                    <td>{{ $vl->usuario }}</td>
                                    <td>
                                        <video width="450" height="150" controls>
                                            <source src="{{ asset('/storage/videos/'.$vl->video) }}" type="video/mp4">
                                            Tu navegador no soporta el videotag :(
                                        </video>
                                        <a href="/storage/videos/llamada-room2022152129150.mp4" download>Descargar video</a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
