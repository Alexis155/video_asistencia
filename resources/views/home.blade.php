@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <calls :user="{{ $user }}"></calls>
</div>
@endsection