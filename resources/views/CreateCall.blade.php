<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/all.css') }}">
    <link rel="stylesheet" href="{{ asset('css/createCall.css') }}">
    <link rel="stylesheet" href="{{ asset('css/sweetalert2.min.css') }}">
    <title>SSPMQRO</title>
</head>
<body>
    <div class="container">
        <div class="card text-center mt-5">
            <div class="card-header bg-sspm">
                <h3 class="text-white">Bienvenido</h3>
            </div>
            <div class="card-body">
                <div class="form-signin">
                    <img class="mb-4" src="{{ asset('img/sspm.png') }}" alt="" width="150" height="150"/>
                    <h1 class="h3 mb-3 font-weight-normal"> Comenzar llamada </h1>
                    <label for="nombre">Por favor indica tu nombre</label>
                    <input type="text" class="form-control" id="nombre" name="nombre" placeholder="Nombre">
                    <button id="call" class="btn btn-lg btn-danger btn-block mt-5" type="button"><i class="fas fa-mobile-alt"></i> Iniciar Llamada</button>
                </div>
                <!-- Se comento por cancelación de implementación de la petición de localización
                <div>
                    <span id="acceso-Ubicacion" class="mt-2 text-muted" style="display: none">
                        <p>¡Ubicación accedida!</p>
                    </span>
                </div>
                -->
                <div>
                    <p class="mt-5 mb-3 text-muted">&copy;SSPMQ</p>
                </div>
            </div>
        </div>

        <!-- inicia Modal -->
	    <div class="modal fade modal-movil" id="myModal" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
            <div class="modal-dialog modal-full" role="document">
                <div class="modal-content">
                    {{-- <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                    </div> --}}
                    <div class="modal-body p-4" id="result">
                        <div class="row">
                        </div>
                        <div id="meet" class="bg-sspm"></div>
                    </div>
                    <div class="modal-footer d-flex justify-content-start">
                        <button id="finish" type="button" class="btn btn-secondary" data-dismiss="modal" onclick="terminarLLamada()">Cerrar</button>
                    </div>
                </div>
            </div>
        </div>

    </div>
</body>
<script src="{{ asset('js/jquery.min.js') }}"></script>
<script src="{{ asset('js/bootstrap.min.js') }}"></script>
<script src="{{ asset('js/all.js') }}"></script>
<script src="{{ asset('js/app.js') }}"></script>
<script src='https://jitsi.sspmqro.gob.mx/external_api.js'></script>
<script src="{{ asset('js/sweetalert2.min.js') }}"></script>
<script src="{{ asset('js/turf.min.js') }}" charset="utf-8"></script>
<script src="{{ asset('js/qro-polygon.js') }}" charset="utf-8"></script>

<script  type="text/javascript">
    
    var api = null;
    var date = new Date();
    var lat = '';
    var long = '';
    var hasPermissions = 'false';
    var user = '';
    const year = date.getFullYear();
    const month = date.getMonth() +1;
    const day = date.getDate();
    const hour =date.getHours();
    const minute = date.getMinutes();
    const milisecons = date.getMilliseconds();
    var roomName = 'room'+year+month+day+hour+minute+milisecons;
    var asistencia = 'asistenciapp'+year+month+day+hour+minute+milisecons;

    //Leer la url y la descompone para saber si existe algun parametro llamado folio
    const url_string = window.location.href;        
    const url = new URL(url_string);
    const folio = url.searchParams.get("folio");
    const prueba = url.searchParams.get("prueba");
    const beneficiado = url.searchParams.get("benef");

    function codeAdderess(){
        const domain = 'jitsi.sspmqro.gob.mx';
        const options = {
            roomName: folio ? asistencia : roomName,
            width: '100%',
            height: 800,
            parentNode: document.querySelector('#meet'),
            userInfo: {
                email: '',
                displayName: user
            },
            configOverwrite: {
            requireDisplayName: false,
            prejoinPageEnabled: false,
            disableDeepLinking: true,
          },
          interfaceConfigOverwrite: {
            MOBILE_APP_PROMO: false,
          }
        };
        api = new JitsiMeetExternalAPI(domain, options);
    }

    function getLocation() {
      if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition, showError, opciones);
      } else { 
        alert("Geolocation is not supported by this browser.");
      }
    }
    
    function showPosition(position) {
        lat = position.coords.latitude;
        long = position.coords.longitude;
        
        hasPermissions = 'true';
        document.getElementById("acceso-Ubicacion").style.display = 'block'; 
    }
    
    function showError(error) {
      switch(error.code) {
        case error.PERMISSION_DENIED:
            //alert("User denied the request for Geolocation.");
            Swal.fire({
              title: '¡Permisos de ubicación rechazados!',
              text: 'Habilite la ubicación para hacer uso de la aplicación',
              icon: 'error'
            })
          break;
        case error.POSITION_UNAVAILABLE:
          //alert("Location information is unavailable.");
          Swal.fire({
              title: '¡Oh no!',
              text: 'No se encuentra disponible la ubicación',
              icon: 'error',
            })
          break;
        case error.TIMEOUT:
            //alert("The request to get user location timed out.");
            Swal.fire({
              title: '¡Oh no!',
              text: 'El tiempo de espera se ha acabado, contacte al administrador del sistema',
              icon: 'error',
            })
          break;
        case error.UNKNOWN_ERROR:
            //alert("An unknown error occurred.");
            Swal.fire({
              title: '¡Oh no!',
              text: 'Parece haber surgido un error, contacte al administrador del sistema',
              icon: 'error',
            })
          break;
      }
    }

    const opciones = {
        enableHighAccuracy: true,
        maximumAge: 5000,
        timeout: 27000
    };

    function terminarLLamada() {
        if (folio != null) {
            location.href ="https://sispum.sspmqro.gob.mx/medidasprot/solicitaasistencia.aspx";
        }
    }

    function entrarConFolio(){
        if (folio != null) {
            beneficiado != null ? $('#nombre').val(folio+'-'+beneficiado) : $('#nombre').val(folio);
            $('#myModal').modal('show');
            user = $('#nombre').val();
            let _data = {
                name: asistencia,
                lat: lat,
                long: long,
                user: user
            }
            $.ajax({
                url:'{{ url('createCall') }}',
                type: 'POST',
                dataType: 'json',
                headers:{
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: _data,
                success: function(data){
                    //console.log(data);
                }
            });
            codeAdderess();
        }
    }

    function videollamadaDePrueba() {
        if (prueba != null) {
            $('#myModal').modal('show');
            user = $('#nombre').val();
            let _data = {
                name: asistencia,
                lat: lat,
                long: long,
                user: user
            }
            codeAdderess();
        }
    }

    $(document).ready(function(){
        if (folio != null) {
            entrarConFolio();
        }else if (prueba != null) {
            videollamadaDePrueba();
        }else {
            //
        }

        //Código comentado por la cancelación de petición de ubicación dentro del estado de queretaro y su futura implementación
        $('#call').click(function(){
            //navigator.geolocation.getCurrentPosition(showPosition);
            //alert('Permisos aceptados?: ' + hasPermissions + ' con coords: ' + lat + ', ' + long );
            /*if (hasPermissions == 'true') {
                //Creamos el poligono del Estado de Querétaro y checamos si las coordenadas se encuentran dentro de ese poligono.
                if (long != '' && lat != '') {
                    var usuario = turf.point([long, lat]);
                    var queretaro = turf.polygon(geodataqro);   

                    var estaEnQro = turf.booleanPointInPolygon(usuario, queretaro); 
                    if (estaEnQro == true) {*/
                        //alert('Está dentro de Qro: ' + estaEnQro);                    
                        $('#myModal').modal('show');
                        user = $('#nombre').val();
                        let _data = {
                            name: roomName,
                            lat: lat,
                            long: long,
                            user: user
                        }
                        $.ajax({
                            url:'{{ url('createCall') }}',
                            type: 'POST',
                            dataType: 'json',
                            headers:{
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            },
                            data: _data,
                            success: function(data){
                                //console.log(data);
                            }
                        });
                        codeAdderess()   
                    /*
                    }else if (estaEnQro == false) {
                        Swal.fire({
                          title: '¡Lo sentimos!',
                          text: 'El uso de este servicio está destinado para el estado de Querétaro',
                          icon: 'error'
                        })
                    }else {
                        alert('En ninguna: ' + estaEnQro);
                    }
                    
                }else {
                    console.log("Error al obtener las coordenadas");
                }
                
            }else {
                Swal.fire({
                title: '¡Importante!',
                text: 'Es necesario aceptar los permisos de ubicación para el uso de la applicación',
                imageUrl: "{{ asset('img/maps-alert.png') }}",
                imageWidth: 200,
                imageHeight: 200,
                imageAlt: 'Custom image',
                })
            }
            */
        });

        $('#finish').click(function(){
            api.dispose();
        })
    });
</script>
</html>
