<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="author" content="Yinka Enoch Adedokun">
<title>Login Page</title>
<link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
<link rel="stylesheet" href="{{ asset('css/login.css') }}">
<link rel="stylesheet" href="{{ asset('css/all.css') }}">
</head>
<body>
<div class="container-fluid">
    <div class="row main-content bg-success text-center">
        <div class="col-md-4 text-center company__info">
            <span class="company__logo"><h2><span> <img src="{{ asset('img/sspm.png') }}" alt=""> </span></h2></span>
            <h4 class="company_title">SSPMQRO</h4>
        </div>
        <div class="col-md-8 col-xs-12 col-sm-12 login_form ">
            <div class="container-fluid">
                <div class="row">
                    <h2>Iniciar sesión</h2>
                </div>
                @error('usuario')
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        <strong>Error!</strong> {{ $message }}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                @enderror
                <div class="row">
                    <form method="POST" action="{{ route('login') }}" class="form-group">
                        @csrf
                        <div class="row">
                            <input id="CveUsuario" type="CveUsuario" class="form__input" name="CveUsuario" placeholder="Usuario" required autocomplete="CveUsuario" autofocus>
                        </div>
                        <div class="row">
                            <input id="PasswdUsr" type="password" class="form__input" name="PasswdUsr" placeholder="Contraseña" required autocomplete="current-PasswdUsr">
                        </div>
                        <div class="row">
                            <button id="xbtn" type="submit" class="btn">Iniciar Sesión</button>
                            {{-- <input type="submit" value="Submit" class="btn"> --}}
                        </div>
                    </form>
                </div>
                <div class="row">
                    {{-- <p>Don't have an account? <a href="{{ route('register') }}">Register Here</a></p> --}}
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Footer -->
<div class="container-fluid text-center footer">
    <p>SSPMQRO</p>
</div>
<script src="{{ asset('js/jquery.min.js') }}"></script>
<script src="{{ asset('js/all.js') }}"></script>
<script src="{{ asset('js/bootstrap.min.js') }}" defer></script>
<script>
    //Leer la url y la descompone para saber si existe algun parametro llamado folio
    const url_string = window.location.href;        
    const url = new URL(url_string);
    const u = url.searchParams.get("u");
    const p = url.searchParams.get("p");
    
    $(document).ready(function(){
        if (u != null && p != null) {
            /*
            $("#CveUsuario").val(u);
            $("#PasswdUsr").val(p);
            $( "#xbtn" ).click();
            */
            let tkn = document.getElementsByName("_token")[0].value;
            let _data = {
                CveUsuario: u,
                PasswdUsr: p,
            }
            $.ajax({
                url:'{{ route('autologin') }}',
                type: 'POST',
                dataType: 'json',
                headers:{
                    'X-CSRF-TOKEN': tkn
                },
                data: _data,
                success: function(data){
                    if (data == true) {
                        window.location.href = '{{ route('home') }}';
                    }else {
                        alert("DATOS INCORRECTOS")
                    }
                },
                error: function(XMLHttpRequest, textStatus, errorThrown) { 
                    alert("Status: " + textStatus); alert("Error: " + errorThrown); 
                }
            });
        }
    });
</script>
</body>




{{-- @extends('layouts.app')

@section('content') --}}
{{-- <div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Login') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('login') }}">
                        @csrf

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <div class="col-md-6 offset-md-4">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                    <label class="form-check-label" for="remember">
                                        {{ __('Remember Me') }}
                                    </label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Login') }}
                                </button>

                                @if (Route::has('password.request'))
                                    <a class="btn btn-link" href="{{ route('password.request') }}">
                                        {{ __('Forgot Your Password?') }}
                                    </a>
                                @endif
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div> --}}

{{-- @endsection --}}
