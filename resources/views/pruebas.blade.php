@extends('layouts.app')

@section('content')
<div class="container-fluid">
    <form method="POST" action="{{ route('video.up') }}" enctype="multipart/form-data" >
        {{ csrf_field() }}
        <div >
        <label>Title</label>
        <input type="text" name="title" placeholder="Enter Title">
        </div>
        <div >
        <label>Choose Video</label>
        <input type="file"  name="video">
        </div>
        <hr>
        <button type="submit" >Submit</button>
    </form>
</div>
@endsection
