<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/all.css') }}">
    <link rel="stylesheet" href="{{ asset('css/createVideoCall.css') }}">
    <link rel="stylesheet" href="{{ asset('css/sweetalert2.min.css') }}">
    <title>SSPMQRO</title>
</head>
<body>
    <nav class="navbar navbar-dark text-white d-flex" style="background-color: #042a45">
        <span class="h1 mr-auto">Video Asistencia SSPMQ</span>
    </nav>
    <main class="py-4">
        <div id="app">
            @yield('content')
        </div>    
        <input id="name" name="name" type="hidden" value="a">
        <input id="code" name="code" type="hidden" value="a"> 
    </main>
</body>
<script src="{{ asset('js/jquery.min.js') }}"></script>
<script src="{{ asset('js/bootstrap.min.js') }}"></script>
<script src="{{ asset('js/all.js') }}"></script>
<script src="{{ asset('js/app.js') }}"></script>
<script src='https://jitsi.sspmqro.gob.mx/external_api.js'></script>
<script src="{{ asset('js/sweetalert2.min.js') }}"></script>
<script src="{{ asset('js/turf.min.js') }}" charset="utf-8"></script>
<script src="{{ asset('js/qro-polygon.js') }}" charset="utf-8"></script>

<script  type="text/javascript">
    
    var api = null;
    var date = new Date();
    var lat = '';
    var long = '';
    var hasPermissions = 'false';
    var user = '';
    const year = date.getFullYear();
    const month = date.getMonth() +1;
    const day = date.getDate();
    const hour =date.getHours();
    const minute = date.getMinutes();
    const milisecons = date.getMilliseconds();
    var roomName = '';

    //Leer la url y la descompone para saber si existe algun parametro llamado folio
    const url_string = window.location.href;        
    const url = new URL(url_string);
    const name = url.searchParams.get("name");
    const code = url.searchParams.get("code");
    document.getElementById("name").value = name;
    document.getElementById("code").value = code;

    const opciones = {
        enableHighAccuracy: true,
        maximumAge: 5000,
        timeout: 27000
    };

    function entrarConUsuario(){
        if (name != null && code != null) {
            let _data = {
                name: name + '-' +day + '-' + month + '-' + year + '-' + code,
                lat: lat,
                long: long,
                user: name
            }
            $.ajax({
                url: '{{ url('createCall') }}',
                type: 'POST',
                dataType: 'json',
                headers:{
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                data: _data,
                success: function(data){
                    //console.log(data);
                }
            });
            console.log( 'create call');
            console.log( '{{ url('createCall') }}');
            const nose = location.protocol + '//' + location.host + '/'; //+ location.pathname;
            console.log(nose);
            /*
            codeAdderess();
            */
        }
    }

    $(document).ready(function(){
        /*
        if (name != null && code != null) {
            entrarConUsuario();
        }else {
            Swal.fire({
                  title: '¡Importante!',
                  text: 'Parametros necesarios para ingresar a la videollamada',
                  imageUrl: "{{ asset('img/maps-alert.png') }}",
                  imageWidth: 200,
                  imageHeight: 200,
                  imageAlt: 'Custom image',
                });
        }*/

        $('#finish').click(function(){
            api.dispose();
        })
    });
</script>
</html>
