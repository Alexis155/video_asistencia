<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCallsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('calls', function (Blueprint $table) {
            $table->id();
            $table->string('room_name');
            $table->integer('status')->default(0)->comment('0:created', '1:in_progres', '2:finished', '3:transfer');
            $table->string('user')->nullable();
            $table->string('lat');
            $table->string('long');
            $table->integer('departament')->nullable()->comment('1:a', '2:b');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('calls');
    }
}
