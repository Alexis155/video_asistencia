<?php

use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Route::get('/prueba_video', function(){
    return view('pruebas');
});

Route::get('/login', [App\Http\Controllers\Login\LoginController::class, 'index']);
Route::post('/login', [App\Http\Controllers\Login\LoginController::class, 'login'])->name('login');
Route::post('/autologin', [App\Http\Controllers\Login\LoginController::class, 'autologin'])->name('autologin');
Route::post('logout', [App\Http\Controllers\Login\LoginController::class, 'logout'])->name('logout');

Route::get('/call', 'App\Http\Controllers\CallController@index');
Route::get('/videocall', 'App\Http\Controllers\CallController@main');
Route::post('/createCall', 'App\Http\Controllers\CallController@store');

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('/despachadores',[App\Http\Controllers\HomeController::class, 'despachadores']);
Route::get('/calls', [App\Http\Controllers\HomeController::class, 'getCalls'])->name('getCalls');
Route::get('/colors', [App\Http\Controllers\HomeController::class, 'getColors'])->name('getColors');
Route::get('/medidas', [App\Http\Controllers\HomeController::class, 'getMedidas'])->name('getMedidas');
Route::get('/link', [App\Http\Controllers\HomeController::class, 'getLink'])->name('getLink');
//Route::get('/medida/{mp}',[App\Http\Controllers\HomeController::class,'findMedida'])->name('findMedida');
Route::get('/medida/{mp}',[App\Http\Controllers\HomeController::class,'findMedida'])->name('findMedida');
Route::get('/medida/{mp}/{benef}',[App\Http\Controllers\HomeController::class,'findMedidaBenef'])->name('findMedidaBenef');
Route::put('/contestar/{id}',[App\Http\Controllers\CallController::class,'update'])->name('contestar');
Route::put('/contestarcrum/{id}',[App\Http\Controllers\CallController::class,'updateCrum'])->name('contestarcrum');
Route::put('/contestaroperador/{id}',[App\Http\Controllers\CallController::class,'updateOperador'])->name('contestaroperador');
Route::put('/contestarmedic/{id}',[App\Http\Controllers\CallController::class,'updateMedic'])->name('contestarmedic');
Route::put('/transferir/{id}', [App\Http\Controllers\CallController::class, 'transferir'])->name('transferir');
Route::get('/historial', [App\Http\Controllers\CallController::class, 'historial'])->name('historial');
Route::put('/terminarllamada/{id}', [App\Http\Controllers\CallController::class, 'terminar']);

Route::post('/guardar-video', [App\Http\Controllers\CallController::class, 'video'])->name('video');
Route::post('/uploadVideo',  [App\Http\Controllers\CallController::class, 'uploadVideo'])->name('video.up');

// Auth::routes();
// Falta controlador y ver si se migra tabla

Route::get('/despachadoress',[App\Http\Controllers\CallController::class, 'despachadores']);
Route::get('/callss/{roomname}', [App\Http\Controllers\CallController::class, 'getCalls'])->name('getCalls');
Route::post('/createVideoCall', 'App\Http\Controllers\CallController@storee');

//

Route::get('/createVideoCallCrum/{name}/{user}', 'App\Http\Controllers\CallController@storeCrum');