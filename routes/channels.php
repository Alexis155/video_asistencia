<?php

use Illuminate\Support\Facades\Broadcast;

/*
|--------------------------------------------------------------------------
| Broadcast Channels
|--------------------------------------------------------------------------
|
| Here you may register all of the event broadcasting channels that your
| application supports. The given channel authorization callbacks are
| used to check if an authenticated user can listen to the channel.
|
*/

Broadcast::channel('App.Models.User.{id}', function ($user, $id) {
    return (int) $user->id === (int) $id;
});

Broadcast::channel('call', function($user){
    foreach($user->moduloVllam as $modulo){
        return (string ) $modulo->pivot->CveSubMod == (string) 'RECEP';
    }
});

Broadcast::channel('contestar', function($user){
    foreach($user->moduloVllam as $modulo){
        return (string ) $modulo->pivot->CveSubMod == (string) 'RECEP' || (string ) trim($modulo->pivot->CveSubMod) == (string) 'CRUM';
    }
});

Broadcast::channel('contestarcrum', function($user){
    foreach($user->moduloVllam as $modulo){
        return (string ) trim($modulo->pivot->CveSubMod) == (string) 'CRUM';
    }
});

Broadcast::channel('contestaroperador', function($user){
    foreach($user->moduloVllam as $modulo){
        return (string ) $modulo->pivot->CveSubMod == (string) 'RECEP';
    }
});

Broadcast::channel('contestarmedic', function($user){
    foreach($user->moduloVllam as $modulo){
        return (string) $modulo->pivot->CveSubMod == (string) 'MEDIC';
    }
});

Broadcast::channel('transferir', function($user){
    foreach($user->moduloVllam as $modulo){
        if( (string ) $modulo->pivot->CveSubMod == (string) 'DG621' || (string ) $modulo->pivot->CveSubMod == (string) 'DG622' || (string ) $modulo->pivot->CveSubMod == (string) 'DG623' || (string ) $modulo->pivot->CveSubMod == (string) 'DG624' || (string ) trim($modulo->pivot->CveSubMod) == (string) 'CRUM' || (string ) trim($modulo->pivot->CveSubMod) == (string) 'MEDIC'){
            return true;
        }
    }
});
